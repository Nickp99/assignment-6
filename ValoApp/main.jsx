const { useState, useEffect } = React;
function option({ setData, baseType }) {
    return fetch(`https://valorant-api.com/v1/${baseType}/`)
        .then((r) => r.json())
        .then((d) => setData(d));
}
function Lioptions({ baseType, data3, baseValue, setImgsrc, setDiscription }) {

    if (data3 && data3.data && baseType === "agents") {
        return (<ul onClick={(e) => {
            e.preventDefault(); if (e.target.tagName === 'LI') {
                setImgsrc(e.target.getAttribute('link'));
                setDiscription(e.target.getAttribute('disc'));
                console.log(link);
            }
        }}>
            <li link={data3.data[baseValue].displayIcon} disc={data3.data[baseValue].description}>display Icon</li>
            <li link={data3.data[baseValue].bustPortrait} disc={data3.data[baseValue].description}>bust Portrait</li>
            {data3.data[baseValue].abilities.map((ability) => (
                <li link={ability.displayIcon} disc={ability.description}>{ability.displayName}
                </li>))}

        </ul>

        )
    } else if (data3 && data3.data && baseType === "weapons") {
        return (<ul onClick={(e) => {
            e.preventDefault(); if (e.target.tagName === 'LI') {
                setImgsrc(e.target.getAttribute('link'));
                setDiscription(e.target.getAttribute('disc'));
                console.log(link);
            }
        }}>
            {data3.data[baseValue].skins
                .filter((skin) => skin.displayIcon !== null)
                .map((skin) => (
                    <li link={skin.displayIcon} disc={skin.displayName}>
                        {skin.displayName}
                    </li>
                ))}</ul>)
    }
    return (<ul></ul>)
}
function MainSec({Imgsrc,discription}){
    return(
        <main>
            <img src={Imgsrc}></img>
            <p>{discription}</p>
        </main>
    )

}
function App() {
    const types = ["agents", "weapons"];
    const [baseType, setBaseType] = useState(types[0]);
    const [discription, setDiscription] = useState(null);
    const [data2, setData] = useState(null);
    const [data3, setData3] = useState(null);
    const [baseValue, setBaseValue] = useState(0);
    const [Imgsrc, setImgsrc] = useState("");

    useEffect(() => {
        option({ setData, baseType });
    }, [baseType]);


    return (<div class="wrapper"><header>
        <h1>Valorant Agent Picker</h1>
        <lable>Valo type</lable>

        <form>

            <select onChange={(e) => {
                setBaseType(e.target.value);
                setData3(null);
                
            }} value={baseType}> {types.map((type) => (
                <option value={type} >
                    {type}
                </option>))}
            </select>

            <select id="options" onChange={(e) => { setBaseValue(e.target.value); setData3(null); }}value={baseValue}>
                {data2 &&
                    data2.data
                        .map((agent, index) => ({
                            ...agent,
                            index,
                        }))
                        .filter((agent) => agent.isPlayableCharacter !== false)
                        .map((agent) => (
                            <option value={agent.index}>
                                {agent.displayName}
                            </option>
                        ))}
            </select>
            <button type="submit" onClick={(e) => {
                e.preventDefault();
                setData3(data2);
            }}>Submit</button>
        </form>
        <p>Selected option:</p>
        {data2 && <Lioptions baseType={baseType} data3={data3} baseValue={baseValue} setImgsrc={setImgsrc} setDiscription={setDiscription} />}
    </header>
    <MainSec Imgsrc={Imgsrc} discription={discription}/>
    </div>);

}
ReactDOM.render(<App />, document.querySelector('#root'));

// https://valorant-api.com/v1/agents/
// https://dash.valorant-api.com/endpoints/agents